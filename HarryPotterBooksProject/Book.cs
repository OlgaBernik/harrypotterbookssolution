﻿using HarryPotterBooksProject.Interfaces;

namespace HarryPotterBooksProject
{
	/// <summary>
	/// Book class
	/// </summary>
	public class Book : IBook
	{
		public Book(int id, string title)
		{
			this.Id = id;
			this.Title = title;
		}

		public int Id { get; private set; }

		public string Title { get; private set; }
	}
}
