﻿using System.Collections.Generic;

using HarryPotterBooksProject.Interfaces;

namespace HarryPotterBooksProject
{
	/// <summary>
	/// Shopping Cart class
	/// </summary>
	public class ShoppingCart : IShoppingCart
	{
		public List<IBook> BooksList { get; set; }

		public List<IBook> CreateEmptyCart()
		{
			this.BooksList = new List<IBook>();
			return this.BooksList;
		}

        public void AddBook(IBook book)
        {
			this.BooksList.Add(book);
        }
	}
}
