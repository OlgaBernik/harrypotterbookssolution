﻿namespace HarryPotterBooksProject.Interfaces
{
	/// <summary>
	/// Book interface
	/// </summary>
	public interface IBook
	{
		int Id { get; }

		string Title { get; }
	}
}
