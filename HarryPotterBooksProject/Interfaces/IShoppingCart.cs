﻿using System.Collections.Generic;

namespace HarryPotterBooksProject.Interfaces
{
	/// <summary>
	/// Shopping Cart interface
	/// </summary>
	public interface IShoppingCart
	{
		List<IBook> BooksList { get; set; }

		List<IBook> CreateEmptyCart();

		void AddBook(IBook book);
	}
}
