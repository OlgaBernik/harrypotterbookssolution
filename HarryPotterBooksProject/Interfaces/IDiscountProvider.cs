﻿using System.Collections.Generic;

namespace HarryPotterBooksProject.Interfaces
{
	/// <summary>
	/// Discount provider interface
	/// </summary>
	public interface IDiscountProvider
	{
		Dictionary<int, double> DiscountsList { get; }

		double SingleBookPrice { get; }
	}
}
