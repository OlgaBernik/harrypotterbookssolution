﻿using System.Collections.Generic;

namespace HarryPotterBooksProject.Interfaces
{
	/// <summary>
	/// Price Calculator interface
	/// </summary>
	public interface IPriceCalculator
	{
		double GetDiscountValue(int uniqueBooksCount, Dictionary<int, double> discountsList);

		double GetAllBooksPrice(List<IBook> books, IDiscountProvider discount);
	}
}
