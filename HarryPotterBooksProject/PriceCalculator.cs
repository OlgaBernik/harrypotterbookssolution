﻿using System.Collections.Generic;
using System.Linq;

using HarryPotterBooksProject.Interfaces;

namespace HarryPotterBooksProject
{
	/// <summary>
	/// Price Calculator class
	/// </summary>
	public class PriceCalculator : IPriceCalculator 
	{
		// public in case we need to get discount value only
		public double GetDiscountValue(int uniqueBooksCount, Dictionary<int, double> discountsList)
		{
			var discountValue = 0.0;
			if (discountsList.ContainsKey(uniqueBooksCount))
			{
				discountValue = discountsList[uniqueBooksCount];
			}

			return discountValue;
		}

		public double GetAllBooksPrice(List<IBook> books, IDiscountProvider discount)
		{
			int uniqueBooksCount = books.Select(x => x.Title).Distinct().Count();
			double discountValue = this.GetDiscountValue(uniqueBooksCount, discount.DiscountsList);

			double finalPrice = ((books.Count - uniqueBooksCount) * discount.SingleBookPrice) + (uniqueBooksCount * (1 - discountValue) * discount.SingleBookPrice);

			return finalPrice;
		}
	}
}
