﻿using System.Collections.Generic;

using HarryPotterBooksProject.Interfaces;

namespace HarryPotterBooksProject
{
	/// <summary>
	/// Discount provider class 
	/// </summary>
	public class DiscountProvider : IDiscountProvider
	{
		public DiscountProvider(Dictionary<int, double> discountsList, double singleBookPrice)
		{
			this.DiscountsList = discountsList;
			this.SingleBookPrice = singleBookPrice;
		}

		public Dictionary<int, double> DiscountsList { get; private set; }

		public double SingleBookPrice { get; private set; }
	}
}
