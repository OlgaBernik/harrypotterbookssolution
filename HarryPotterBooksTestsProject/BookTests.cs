﻿using HarryPotterBooksProject;
using HarryPotterBooksProject.Interfaces;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HarryPotterBooksTestsProject
{
	/// <summary>
	/// Book tests
	/// </summary>
	[TestClass]
	public class BookTests
	{
		/// <summary>
		/// Create the book test.
		/// </summary>
		[TestMethod]
		public void CreateBookTest()
		{
			const int bookId = 1;
			var booksTitles = TestsHelper.BooksTitles;
			string bookTitle = booksTitles[bookId];

			IBook bookForCreate = new Book(bookId, bookTitle);

			Assert.AreEqual(bookForCreate.Id, bookId);
			Assert.AreEqual(bookForCreate.Title, bookTitle);
		}
	}
}
