﻿using System.Collections.Generic;
using System.Linq;

using HarryPotterBooksProject;
using HarryPotterBooksProject.Interfaces;

using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HarryPotterBooksTestsProject
{
	/// <summary>
	/// Shopping Cart tests
	/// </summary>
	[TestClass]
	public class ShoppingCartTests
	{
		private IUnityContainer container;

		private IShoppingCart cart;

		/// <summary>
		/// Add the book to cart test.
		/// </summary>
		[TestMethod]
		public void AddBookToCartTest()
		{
			const int bookId = 1;
			var booksTitles = TestsHelper.BooksTitles;
			string bookTitle = booksTitles[bookId];

			List<IBook> noCart = this.cart.BooksList;
			Assert.AreEqual(noCart, null);

			List<IBook> emptyCart = this.cart.CreateEmptyCart();
			Assert.AreEqual(emptyCart.Count, 0);

			IBook bookForBuy = new Book(bookId, bookTitle);
			this.cart.AddBook(bookForBuy);

			List<IBook> oneBookCart = this.cart.BooksList;
			Assert.AreEqual(oneBookCart.Count, 1);
			Assert.AreEqual(oneBookCart.First(), bookForBuy);
		}

		/// <summary>
		/// The set up.
		/// </summary>
		[TestInitialize]
		public void SetUp()
		{
			this.container = new UnityContainer();
			this.container.RegisterType<IShoppingCart, ShoppingCart>();
			this.cart = this.container.Resolve<IShoppingCart>();
		}

		/// <summary>
		/// The tear down.
		/// </summary>
		[TestCleanup]
		public void TearDown()
		{
			this.cart = null;
			this.container.Dispose();
		}
	}
}
