﻿using System.Collections.Generic;
using System.Linq;

using HarryPotterBooksProject;
using HarryPotterBooksProject.Interfaces;

using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HarryPotterBooksTestsProject
{
	/// <summary>
	/// Unit tests
	/// </summary>
	[TestClass]
	public class IntegrationTests
	{
		private IUnityContainer container;

		private IPriceCalculator priceCalculator;
		private IShoppingCart cart;

		/// <summary>
		/// Buy the four books two unique test.
		/// </summary>
		[TestMethod]
		public void BuyFourBooksTwoUniqueTest()
		{
			var booksTitles = TestsHelper.BooksTitles;
			var discounts = TestsHelper.Discounts;
			double price = TestsHelper.SingleBookPrice;

			const int bookId1 = 1;
			const int bookId2 = 2;

			// create books
			IBook firstBook = new Book(bookId1, booksTitles[bookId1]);
			IBook oneMoreFirstBook = new Book(bookId1, booksTitles[bookId1]);
			IBook secondBook = new Book(bookId2, booksTitles[bookId2]);
			IBook oneMoreSecondBook = new Book(bookId2, booksTitles[bookId2]);

			// add books to cart
			this.cart.CreateEmptyCart();
			this.cart.AddBook(firstBook);
			this.cart.AddBook(oneMoreFirstBook);
			this.cart.AddBook(secondBook);
			this.cart.AddBook(oneMoreSecondBook);

			// set discounts and price
			var discount = new DiscountProvider(discounts, price);

			// final price calculation
			double allBooksPrice = this.priceCalculator.GetAllBooksPrice(this.cart.BooksList, discount);
			Assert.AreEqual(allBooksPrice, 31.2);
		}

		/// <summary>
		/// Buy the seven books unique only test.
		/// </summary>
		[TestMethod]
		public void BuySevenBooksUniqueOnlyTest()
		{
			var booksTitles = TestsHelper.BooksTitles;
			var discounts = TestsHelper.Discounts;
			double price = TestsHelper.SingleBookPrice;

			const int bookId1 = 1;
			const int bookId2 = 2;
			const int bookId3 = 3;
			const int bookId4 = 4;
			const int bookId5 = 5;
			const int bookId6 = 6;
			const int bookId7 = 7;

			// create books
			IBook firstBook = new Book(bookId1, booksTitles[bookId1]);
			IBook secondBook = new Book(bookId2, booksTitles[bookId2]);
			IBook thirdBook = new Book(bookId3, booksTitles[bookId3]);
			IBook fourthBook = new Book(bookId4, booksTitles[bookId4]);
			IBook fifthBook = new Book(bookId5, booksTitles[bookId5]);
			IBook sixthBook = new Book(bookId6, booksTitles[bookId6]);
			IBook seventhBook = new Book(bookId7, booksTitles[bookId7]);

			// add books to cart
			this.cart.CreateEmptyCart();
			this.cart.AddBook(firstBook);
			this.cart.AddBook(secondBook);
			this.cart.AddBook(thirdBook);
			this.cart.AddBook(fourthBook);
			this.cart.AddBook(fifthBook);
			this.cart.AddBook(sixthBook);
			this.cart.AddBook(seventhBook);
			List<IBook> sevenBooksCart = this.cart.BooksList;
			Assert.AreEqual(sevenBooksCart.Count, 7);
			Assert.AreEqual(sevenBooksCart.First(), firstBook);
			Assert.AreEqual(sevenBooksCart.Last(), seventhBook);

			// set discounts and price
			var discount = new DiscountProvider(discounts, price);

			// final price calculation
			double allBooksPrice = this.priceCalculator.GetAllBooksPrice(this.cart.BooksList, discount);
			Assert.AreEqual(allBooksPrice, 36.4);
		}

		/// <summary>
		/// Buy the four books two unique test.
		/// </summary>
		[TestMethod]
		public void BuyFourTheSameBooksTest()
		{
			var booksTitles = TestsHelper.BooksTitles;
			var discounts = TestsHelper.Discounts;
			double price = TestsHelper.SingleBookPrice;

			const int bookId5 = 5;

			// create books
			IBook fifthBook = new Book(bookId5, booksTitles[bookId5]);

			// add books to cart
			this.cart.CreateEmptyCart();
			this.cart.AddBook(fifthBook);
			this.cart.AddBook(fifthBook);
			this.cart.AddBook(fifthBook);
			this.cart.AddBook(fifthBook);

			// set discounts and price
			var discount = new DiscountProvider(discounts, price);

			// final price calculation
			double allBooksPrice = this.priceCalculator.GetAllBooksPrice(this.cart.BooksList, discount);
			Assert.AreEqual(allBooksPrice, 32);
		}

		/// <summary>
		/// Buy ten books seven unique test.
		/// </summary>
		[TestMethod]
		public void BuyTenBooksSevenUniqueTest()
		{
			var booksTitles = TestsHelper.BooksTitles;
			var discounts = TestsHelper.Discounts;
			double price = TestsHelper.SingleBookPrice;

			const int bookId1 = 1;
			const int bookId2 = 2;
			const int bookId3 = 3;
			const int bookId4 = 4;
			const int bookId5 = 5;
			const int bookId6 = 6;
			const int bookId7 = 7;

			// create books
			IBook firstBook = new Book(1, booksTitles[bookId1]);
			IBook secondBook = new Book(bookId2, booksTitles[bookId2]);
			IBook thirdBook = new Book(bookId3, booksTitles[bookId3]);
			IBook fourthBook = new Book(bookId4, booksTitles[bookId4]);
			IBook fifthBook = new Book(bookId5, booksTitles[bookId5]);
			IBook sixthBook = new Book(bookId6, booksTitles[bookId6]);
			IBook seventhBook = new Book(bookId7, booksTitles[bookId7]);

			// add books to cart
			this.cart.CreateEmptyCart();
			this.cart.AddBook(firstBook);
			this.cart.AddBook(secondBook);
			this.cart.AddBook(thirdBook);
			this.cart.AddBook(thirdBook);
			this.cart.AddBook(thirdBook);
			this.cart.AddBook(fourthBook);
			this.cart.AddBook(fifthBook);
			this.cart.AddBook(fifthBook);
			this.cart.AddBook(sixthBook);
			this.cart.AddBook(seventhBook);
			List<IBook> tenBooksCart = this.cart.BooksList;
			Assert.AreEqual(tenBooksCart.Count, 10);
			Assert.AreEqual(tenBooksCart.First(), firstBook);
			Assert.AreEqual(tenBooksCart.Last(), seventhBook);

			// set discounts and price
			var discount = new DiscountProvider(discounts, price);

			// final price calculation
			double allBooksPrice = this.priceCalculator.GetAllBooksPrice(this.cart.BooksList, discount);
			Assert.AreEqual(allBooksPrice, 60.4);
		}

		/// <summary>
		/// Buy one book test.
		/// </summary>
		[TestMethod]
		public void BuyOneBookTest()
		{
			var booksTitles = TestsHelper.BooksTitles;
			var discounts = TestsHelper.Discounts;
			double price = TestsHelper.SingleBookPrice;

			const int bookId1 = 1;

			// create books
			IBook firstBook = new Book(1, booksTitles[bookId1]);

			// add books to cart
			this.cart.CreateEmptyCart();
			this.cart.AddBook(firstBook);
			List<IBook> oneBooksCart = this.cart.BooksList;
			Assert.AreEqual(oneBooksCart.Count, 1);
			Assert.AreEqual(oneBooksCart.First(), firstBook);

			// set discounts and price
			var discount = new DiscountProvider(discounts, price);

			// final price calculation
			double allBooksPrice = this.priceCalculator.GetAllBooksPrice(this.cart.BooksList, discount);
			Assert.AreEqual(allBooksPrice, 8);
		}

		/// <summary>
		/// The set up.
		/// </summary>
		[TestInitialize]
		public void SetUp()
		{
			this.container = new UnityContainer();

			this.container.RegisterType<IPriceCalculator, PriceCalculator>();
			this.priceCalculator = this.container.Resolve<IPriceCalculator>();

			this.container.RegisterType<IShoppingCart, ShoppingCart>();
			this.cart = this.container.Resolve<IShoppingCart>();
		}

		/// <summary>
		/// The tear down.
		/// </summary>
		[TestCleanup]
		public void TearDown()
		{
			this.priceCalculator = null;
			this.cart = null;
			this.container.Dispose();
		}
	}
}
