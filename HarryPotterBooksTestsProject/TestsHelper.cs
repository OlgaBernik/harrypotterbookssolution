﻿using System.Collections.Generic;

namespace HarryPotterBooksTestsProject
{
	public static class TestsHelper
	{
		public static Dictionary<int, double> Discounts
		{
			get
			{
				return new Dictionary<int, double>
				{
					{ 2, 0.05 },
					{ 3, 0.1 },
					{ 4, 0.15 },
					{ 5, 0.25 },
					{ 6, 0.3 },
					{ 7, 0.35 }
				};
			}
		}

		public static double SingleBookPrice
		{
			get
			{
				return 8;
			}
		}

		public static Dictionary<int, string> BooksTitles
		{
			get
			{
				return new Dictionary<int, string>
					       {
						       { 1, "The Philosopher's Stone" },
						       { 2, "The Chamber of Secrets" },
						       { 3, "The Prisoner of Azkaban" },
						       { 4, "The Goblet of Fire" },
						       { 5, "The Order of the Phoenix" },
						       { 6, "The Half-Blood Prince" },
						       { 7, "The Deathly Hallows" },
					       };
			}
		}
	}
}
