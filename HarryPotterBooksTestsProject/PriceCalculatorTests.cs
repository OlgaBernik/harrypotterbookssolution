﻿using System.Collections.Generic;

using HarryPotterBooksProject;
using HarryPotterBooksProject.Interfaces;

using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HarryPotterBooksTestsProject
{
	/// <summary>
	/// Book tests
	/// </summary>
	[TestClass]
	public class PriceCalculatorTests
	{
		private IUnityContainer container;

		private IPriceCalculator priceCalculator;

		/// <summary>
		///  Calculate discount test.
		/// </summary>
		[TestMethod]
		public void CalculateDiscountTest()
		{
			Dictionary<int, double> discountValues = TestsHelper.Discounts;

			double zeroDiscount = this.priceCalculator.GetDiscountValue(1, discountValues);
			Assert.AreEqual(zeroDiscount, 0);

			double twoUniqueDiscount = this.priceCalculator.GetDiscountValue(2, discountValues);
			Assert.AreEqual(twoUniqueDiscount, discountValues[2]);

			double fiveUniqueDiscount = this.priceCalculator.GetDiscountValue(5, discountValues);
			Assert.AreEqual(fiveUniqueDiscount, discountValues[5]);
		}

		/// <summary>
		/// The set up.
		/// </summary>
		[TestInitialize]
		public void SetUp()
		{
			this.container = new UnityContainer();
			this.container.RegisterType<IPriceCalculator, PriceCalculator>();
			this.priceCalculator = this.container.Resolve<IPriceCalculator>();
		}

		/// <summary>
		/// The tear down.
		/// </summary>
		[TestCleanup]
		public void TearDown()
		{
			this.priceCalculator = null;
			this.container.Dispose();
		}
	}
}
