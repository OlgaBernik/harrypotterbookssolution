﻿using HarryPotterBooksProject;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HarryPotterBooksTestsProject
{
	/// <summary>
	/// Book tests
	/// </summary>
	[TestClass]
	public class DiscountProviderTests
	{
		/// <summary>
		/// Discount and price test.
		/// </summary>
		[TestMethod]
		public void CreateDiscountAndPriceTest()
		{
			var discounts = TestsHelper.Discounts;
			double price = TestsHelper.SingleBookPrice;

			var discount = new DiscountProvider(discounts, price);
			var filledDiscountList = discount.DiscountsList;

			Assert.AreNotEqual(filledDiscountList, null);
			Assert.AreEqual(filledDiscountList.Count, discounts.Count);
			Assert.AreEqual(filledDiscountList, discounts);

			var filledPrice = discount.SingleBookPrice;
			Assert.AreEqual(filledPrice, price);
		}
	}
}
